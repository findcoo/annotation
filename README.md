##Annotation 오토와이어링

1. *디렉토리 구성*
```
./src/
└── main
    ├── java
    │   └── annotation
    │       ├── Guitar.java
    │       ├── Instrumentalist.java
    │       ├── Instrument.java
    │       ├── Main.java
    │       └── Performer.java
    └── resources
        └── annotation.xml

```
---
2. *주의 및 참고*
	+ @Autowire 애너테이션을 이용해 오토와이어링의 xml 설정을 최소화한다.
	+ setter 역활을 하는 모든 메소드에 사용할 수 있다. (생성자 포함)
	+ 만약에 와이어링 대상이 두개 이상 존재할 경우 혹은 없을 경우 오류가 발생한다.
	+ 위의 사항을 방지하기위해서 `@Autowired(require=false)` 형식의 null 허용을 사용한다.
	+ 이 외에도 한정자를 사용해 선택적으로 와이어링하는 방법을 사용한다.
	
---



package annotation;

public interface Instrument {
	public void play();
}

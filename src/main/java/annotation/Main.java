package annotation;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

	public static void main(String args[]) {
		ApplicationContext acx = new ClassPathXmlApplicationContext("annotation.xml");
		Performer performer = (Performer)acx.getBean("jus");
		performer.perform();

	}
}

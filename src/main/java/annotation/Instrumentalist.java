package annotation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class Instrumentalist implements Performer{

	private Instrument instrument;

	public Instrumentalist() {
	}

	@Override
	public void perform() {
		// TODO Auto-generated method stub
		instrument.play();

	}

	/**
	 * @return the instrument
	 */
	public Instrument getInstrument() {
		return instrument;
	}

	/**
	 * @param instrument the instrument to set
	 */
	@Autowired(required=false)
	@Qualifier("guitar")
	public void setInstrument(Instrument instrument) {
		this.instrument = instrument;
	}
}
